package Github::Webhook::Web::Dispatcher;
use strict;
use warnings;
use utf8;
use Amon2::Web::Dispatcher::RouterBoom;

get '/' => sub {
    my $c = shift;
    my $job_count = $c->qudo->job_count('Github::Webhook::Worker');
    $c->render_json({job_count => $job_count});
};

post '/' => sub {
    my $c = shift;
    my $github = $c->github
        or return $c->render_json({status => 400, message => "invalid request"});
    $c->qudo->enqueue("Github::Webhook::Worker", {arg => $github});
    $c->render_json({status => 200, message => "Successfully queued"});
};


1;
