package Github::Webhook::Web;
use strict;
use warnings;
use utf8;
use parent qw(Github::Webhook Amon2::Web);

use Github::Webhook::Web::Dispatcher;
sub dispatch {
    my $disp = Github::Webhook::Web::Dispatcher->dispatch($_[0])
        or die "response is not generated";
    return $disp;
}

__PACKAGE__->load_plugins(
    'Web::Text',
    'Web::JSON' => {status_code_field => 'status'},
    'Web::Github::Webhook' => {allow => __PACKAGE__->config->{github_allow}},
);

__PACKAGE__->add_trigger(
    AFTER_DISPATCH => sub {
        my ( $c, $res ) = @_;
        # http://blogs.msdn.com/b/ie/archive/2008/07/02/ie8-security-part-v-comprehensive-protection.aspx
        $res->header( 'X-Content-Type-Options' => 'nosniff' );
        # http://blog.mozilla.com/security/2010/09/08/x-frame-options/
        $res->header( 'X-Frame-Options' => 'DENY' );
        # Cache control.
        $res->header( 'Cache-Control' => 'private' );
    },
);

1;
