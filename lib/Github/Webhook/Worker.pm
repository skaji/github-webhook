package Github::Webhook::Worker;
use strict;
use warnings;
use parent 'Qudo::Worker';
use Cwd 'abs_path';
use File::Path 'mkpath';
use File::Spec::Functions qw(catdir catfile);
use Git::Repository;
use Log::Minimal env_debug => "GITHUB_WEBHOOK_DEBUG";
use Data::Dumper;

sub max_retries { 3  }
sub retry_delay { 10 }
sub set_job_status { 1 }

sub c { Github::Webhook->context }

sub work {
    my ($class, $job) = @_;

    my $github  = $job->arg;
    my $event   = $github->{event};
    my $payload = $github->{payload};

    my $url = ($payload->{repository} || +{})->{url};

    debugf "job start event=%s, url=%s", $event, $url || "";
    my $git_dir = $class->git_dir($url)
        or return $job->abort;

    if (-f catfile($git_dir, "HEAD")) {
        debugf "already exists %s/HEAD, thus do git remote update", $git_dir;
        my $git = Git::Repository->new( git_dir => $git_dir );
        $git->run(remote => "update");
    } else {
        debugf "maybe first time, thus do git clone --mirror %s", $git_dir;
        Git::Repository->run( clone => "--mirror", $git_dir );
    }
    return $job->complited;
}

sub git_dir {
    my ($class, $url) = @_;
    if (!$url) {
        warnf "payload.repository.url is missing";
        return;
    }

    # https://github.com/shoichikaji/dummy.git -> shoichikaji/dummy
    my ($dir) = $url =~ m{/([^/]+/[^/]+)(?:\.git)?$};

    if ($dir =~ /\0/ || $dir =~ /\.\./) {
        warnf "payload.repository.url contains invalid string: $url";
        return;
    }

    my $git_dir = abs_path( catdir $class->c->config->{work_dir}, $dir );
    mkpath $git_dir;
    if (!-d $git_dir) {
        warnf "cannot mkpath $git_dir";
        return;
    }
    return $git_dir;
}


1;
