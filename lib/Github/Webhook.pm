package Github::Webhook;
use strict;
use warnings;
use utf8;
use Qudo;
use parent qw(Amon2);
__PACKAGE__->make_local_context();

sub mode_name {
    $ENV{PLACK_ENV} || 'development';
}

sub qudo {
    my $c = shift;
    $c->{qudo} ||= Qudo->new(
        databases => $c->config->{qudo_databases},
        default_hooks => ["Qudo::Hook::Serialize::JSON"],
        manager_abilities => ["Github::Webhook::Worker"],
    );
}

1;
