package t::Util;
use strict;
use warnings;
BEGIN { $ENV{PLACK_ENV} = 'test' }
use Test::More;

{
    # utf8 hack.
    binmode Test::More->builder->$_, ":utf8" for qw/output failure_output todo_output/;
    no warnings 'redefine';
    my $code = \&Test::Builder::child;
    *Test::Builder::child = sub {
        my $builder = $code->(@_);
        binmode $builder->output,         ":utf8";
        binmode $builder->failure_output, ":utf8";
        binmode $builder->todo_output,    ":utf8";
        return $builder;
    };
}
{
    unlink 'db/test.db' if -f 'db/test.db';
    system("sqlite3 db/test.db < db/schema.sql");
}

1;
