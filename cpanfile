requires 'Amon2';
requires 'DBD::SQLite';
requires 'JSON';
requires 'JSON::XS';
requires 'Qudo';
requires 'Router::Boom';
requires 'Starlet';
requires 'Log::Minimal';
requires 'Git::Repository';

requires 'Amon2::Plugin::Web::Github::Webhook', 0, git => 'git://github.com/shoichikaji/Amon2-Plugin-Web-Github-Webhook.git';
requires 'Amon2::Plugin::Web::Text', 0, git => 'git://github.com/shoichikaji/Amon2-Plugin-Web-Text.git';
requires 'Plack::Middleware::AccessLog::RotateLogs', 0, git => 'git://github.com/shoichikaji/Plack-Middleware-AccessLog-RotateLogs.git';
