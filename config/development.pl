use strict;
use warnings;
no warnings 'void';
use File::Spec;
use File::Basename ();
my $base   = File::Basename::dirname(__FILE__) . "/..";
my ($mode) = __FILE__ =~ m{([^/.]+)\.pl$};
my $dbname = "$base/db/$mode.db";

{
    qudo_databases => [
        {dsn => "dbi:SQLite:$dbname", username => "", password => ""},
    ],
    github_allow => ['0.0.0.0/0'],
    work_dir => "$base/work",
}
